

<?php

if(isSet($_post['SUBMIT'])){

    //collect form data
    $name = $_POST['name'];
    $gender = $_POST['gender'];
    $phone = $_POST['phone'];
    $email = $_POST['email'];
    $nationality = $_POST['nationality'];
    $educationalBackground = $_POST['educationalBackground'];
    //check name is set
echo $name;

        # Title of the CSV
        $Content = "Name, Gender, Phone, Email, Nationality, EducationalBackground \n";

        //set the data of the CSV


        $Content .= "$name, $gender $phone $email $nationality $educationalBackground\n";

        # set the file name and create CSV file
        //$FileName = "csv\export\uploads\formdata-".date("d-m-y-h:i:s").".csv";
        $FileName = "csv\export\uploads\formdata-".strToTime("now")."csv";
        header('Content-Type: application/csv');
        header('Content-Disposition: attachment; filename="' . $FileName . '"');
        echo $Content;
        exit();
    }
    else
    {echo "i am in the else section";}
?>


<!DOCTYPE html>
<!-- JSFormValidation.html -->
<html lang="en">
<head>
  <meta charset="utf-8">
  <!--     <link href="{{asset('bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">  -->

<link href="{{asset('bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
<link href="{{asset('registrationForm/css/registrationFormValidation.css')}}" rel="stylesheet">
<!--<link href="{{asset('registrationForm/js/registrationFormValidation.js')}}" rel="javascript"> -->


  <!--<link rel="stylesheet" href="JSFormValidation.css">-->
  <!--<script src="JSFormValidation.js"></script>-->
  <script src="{{asset('registrationForm/js/registrationFormValidation.js')}}"></script>
  <script src="{{asset('registrationForm/js/date.js')}}"></script>
  <script src="{{asset('registrationForm/js/datePicker.js')}}"></script>

</head>

<body>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Register</div>
                <div class="panel-body">
                    <form class="form-horizontal" form id="formTest" role="form" method="post" action="">

    <table>
    <tr>
      <td><label for="txtName">Name<span class="required">*</span></label></td>
      <td><input type="text" id="txtName" name="name"></td>
      <td id="elmNameError" class="errorMsg">&nbsp;</td>
    </tr>

    <tr>
          <td>Gender<span class="required">*</span></td>
          <td><label><input type="radio" name="gender" value="m">Male</label>
              <label><input type="radio" name="gender" value="f">Female</label></td>
          <td id="elmGenderError" class="errorMsg">&nbsp;</td></tr>
    </tr>

    <tr>
          <td><label for="txtPhone">Phone<span class="required">*</span></label></td>
          <td><input type="text" id="txtPhone" name="phone"></td>
          <td id="elmPhoneError" class="errorMsg">&nbsp;</td>
    </tr>

    <tr>
          <td><label for="txtEmail">Email<span class="required">*</span></label></td>
          <td><input type="text" id="txtEmail" name="email"></td>
          <td id="elmEmailError" class="errorMsg">&nbsp;</td>
    </tr>

    <tr>
      <td><label for="txtPassword">Password (6-8 characters)<span class="required">*</span></label></td>
      <td><input type="password" id="txtPassword" name="password"></td>
      <td id="elmPasswordError" class="errorMsg">&nbsp;</td>
    </tr>

    <tr>
      <td><label for="txtPWVerified">Verify Password<span class="required">*</span></label></td>
      <td><input type="password" id="txtPWVerified" name="pwVerified"></td>
      <td id="elmPWVerifiedError" class="errorMsg">&nbsp;</td>
    </tr>

    <tr>
      <td><label for="txtAddress">Address</label></td>
      <td><input type="text" id="txtAddress" name="address"></td>
      <td id="elmAddressError" class="errorMsg">&nbsp;</td>
    </tr>

    <tr>
      <td>Nationality<span class="required">*</span></td>
      <td><select id="selNationality" name="nationality">
            <option value="" selected>Please select...</option>
            <option value="am">American</option>
            <option value="au">Australian</option>
            <option value="ch">Chinese</option>
            <option value="eu">European</option>
            <option value="in">Indian</option>
            <option value="np">Nepali</option>
          </select></td>
      <td id="elmCountryError" class="errorMsg">&nbsp;</td>
    </tr>



    <tr>
      <td>Education Background<span class="required">*</span></td>
      <td><select id="selEducationBackground" name="educationalBackground">
            <option value="" selected>Please select...</option>
            <option value="ec">Early childhood education</option>
            <option value="pe">Primary education</option>
            <option value="se">Secondary education</option>
            <option value="he">Higher education</option>
          </select></td>
      <td id="elmEducationBackgroundError" class="errorMsg">&nbsp;</td>
    </tr>

    <tr>
      <td>&nbsp;</td>
      <td><input type="submit" value="SUBMIT" id="btnSubmit">&nbsp;
          <input type="reset" value="CLEAR" id="btnReset"></td>
      <td>&nbsp;</td>
    </tr>
    </table>
  </form>
</body>
</html>



