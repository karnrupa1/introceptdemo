<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/register', 'registerController@index');
Route::get('/form', 'FormController@create');
//Route::post('/',['as' => 'store', 'uses' => 'FormController@store']);
/*Route::post('csvdata', ['as' => 'csvdata_index', 'uses' => 'CsvController@index']);*/
Route::post('formdata', ['as' => 'formdata_store', 'uses' => 'FormController@store']);
Route::post('csvdata', ['as' => 'csvdata_index', 'uses' => 'CsvController@index']);

Route::get('/dob', function () {return view('dob');});

