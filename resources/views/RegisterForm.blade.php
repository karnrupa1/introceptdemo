@extends('layouts.app')
@section('content')

<div class = "container">
  <div class="row">
    <div class="col-md-8 col-md-offset-2 ">
      <div class ="panel panel-default">
        <div class ="panel-heading>Register</div>
          <div class ="panel-body">
            <ul>
              @foreach($errors->all() as $error)
                <li style='color:#ff0000'>{{ $error }}</li>
              @endforeach
            </ul>
            <div class ="container">
              @if(Session::has('flash_message'))
                <div class="alert alert_sucess">{{Session::get('flash_message')}}</div>
              @endif
            </div>
              {!! Form::open(array('route' => 'formdata_store', 'class' => 'form', 'method' => 'POST')) !!}
                <div class="form-group">
                  {!! Form::label('Name') !!}
                  {!! Form::text('name', '', ['class'=>'form-control']) !!}
                </div>
                <div class="form-group">
                  {!! Form::label('gender:') !!}
                  {!! Form::label('male') !!}
                  {{ Form::radio('gender', 'male') }}
                  {!! Form::label('female') !!}
                  {{ Form::radio('gender', 'female') }}
                </div>
                <div class="form-group">
                  {!! Form::label('phone') !!}
                  {!! Form::text('phone', '', ['class'=>'form-control']) !!}
                </div>
                <div class="form-group">
                  {!! Form::label('email')!!}
                  {!! Form::text('email', '', ['class'=>'form-control']) !!}
                </div>
                <div class="form-group">
                  {!! form::label('address') !!}
                  {!! form::text('address', '', ['class'=>'form-control']) !!}
                </div>
                <div class="form-group">
                  {!! form::label('Nationality') !!}
                  {!! Form::select('nationality', array('' => 'select', 'American'=> 'American', 'Australian'=> 'Australian', 'Chinese'=> 'Chinese', '
                                Indian'=> 'Indian', 'Nepali'=> 'Nepali'), null, ['class'=>'form-control']) !!}
                </div>
                <div class="form-group">{!! form::label('Educational Background') !!}
                  {!! Form::select('educationbackground', array('' => 'select', 'Early childhood education'=> 'Early childhood education', 'Primary education'=> 'Primary education', 'Secondary education'=> 'Secondary education',
                   'Higher education'=> 'Higher education', 'Graduation'=> 'Graduation', 'Doctrate'=> 'Doctrate', 'PostDoctrate'=> 'PostDoctrate'), null, ['class'=>'form-control']) !!}
                </div>
                <div class="form-group">
                  {!! form::label('DOB') !!}
                  {!! Form::text('dob', '', array('' => "DD-MM-YY", 'id' => 'datepicker', 'class'=>'form-control')) !!}
                </div>
                <div class="form-group">
                  {!! Form::submit('Save', array('class'=>'btn btn-primary')) !!}
                  {!! Form::reset('Clear form', ['class' => 'btn btn-danger']) !!}
                  {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
  </div> {{--row--}}
</div> {{--container--}}
@endsection
