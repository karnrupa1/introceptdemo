@extends('layouts.app')
@section('content')

<!DOCTYPE html>
<!-- JSFormValidation.html -->
<html lang="en">
  <head>
    <meta charset="utf-8">
    <!--     <link href="{{asset('bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">  -->

    <link href="{{asset('bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('registrationForm/css/registrationFormValidation.css')}}" rel="stylesheet">

    <script src="{{asset('registrationForm/js/registrationFormValidation.js')}}"></script>
    <script src="{{asset('registrationForm/js/date.js')}}"></script>
    <script src="{{asset('registrationForm/js/datePicker.js')}}"></script>

  </head>
  <body>
    <div class="container">
      <div class="row">
        <div class="col-md-8 col-md-offset-2">
          <div class="panel panel-default">
            <div class="panel-heading">Register</div>
              <div class="panel-body">
                <form class="form-horizontal" form id="formTest" role="form"
                method="POST" action="{{asset('csvform.php')}}">
    <table>
      <tr>
        <td><label for="txtName">Name<span class="required">*</span></label></td>
        <td><input type="text" id="txtName" name="name"></td>
        <td id="elmNameError" class="errorMsg">&nbsp;</td>
      </tr>
      <tr>
        <td>
          <label for="txtGender">Gender<span class="required">*</span>
        </td>
        <td>
          <label><input type="radio" name="gender" value="m">Male</label>
          <label><input type="radio" name="gender" value="f">Female</label></td>
        <td id="elmGenderError" class="errorMsg">&nbsp;</td></tr>
      </tr>
      <tr>
        <td><label for="txtPhone">Phone<span class="required">*</span></label></td>
        <td><input type="text" id="txtPhone" name="phone"></td>
        <td id="elmPhoneError" class="errorMsg">&nbsp;</td>
      </tr>
      <tr>
        <td><label for="txtEmail">Email<span class="required">*</span></label></td>
        <td><input type="text" id="txtEmail" name="email"></td>
        <td id="elmEmailError" class="errorMsg">&nbsp;</td>
      </tr>
      <tr>
        <td><label for="txtPassword">Password (6-8 characters)<span class="required">*</span></label></td>
        <td><input type="password" id="txtPassword" name="password"></td>
        <td id="elmPasswordError" class="errorMsg">&nbsp;</td>
      </tr>
      <tr>
        <td><label for="txtPWVerified">Verify Password<span class="required">*</span></label></td>
        <td><input type="password" id="txtPWVerified" name="pwVerified"></td>
        <td id="elmPWVerifiedError" class="errorMsg">&nbsp;</td>
      </tr>
      <tr>
        <td><label for="txtAddress">Address</label></td>
        <td><input type="text" id="txtAddress" name="address"></td>
        <td id="elmAddressError" class="errorMsg">&nbsp;</td>
      </tr>
      <tr>
        <td><label for="txtNationality">Nationality<span class="required">*</span></td>
        <td>
          <select id="selNationality" name="nationality">
            <option value="" selected>Please select...</option>
            <option value="American">American</option>
            <option value="Australian">Australian</option>
            <option value="Chinese">Chinese</option>
            <option value="European">European</option>
            <option value="Indian">Indian</option>
            <option value="Nepali">Nepali</option>
          </select>
        </td>
        <td id="elmCountryError" class="errorMsg">&nbsp;</td>
      </tr>
      <tr>
        <td>
          <label for="txtEducationalBackground">Educational Background<span class="required">*</span></td>
            <td>
              <select id="selEducationBackground" name="educationalBackground">
                <option value="" selected>Please select...</option>
                <option value="Early childhood education">Early childhood education</option>
                <option value="Primary education">Primary education</option>
                <option value="Secondary education">Secondary education</option>
                <option value="Higher education">Higher education</option>
                <option value="Graduation">Graduation</option>
                <option value="Doctrate">Doctrate</option>
                <option value="PostDoctrate">PostDoctrate</option>
              </select>
            </td>
          <td id="elmEducationBackgroundError" class="errorMsg">&nbsp;</td>
      </tr>
      <tr>
        <td>
          <label for="txtDob" name="dob">DOB<span class="required">*</span></td>
        <td>
          <label for="txtYear">Year: <input type="text" name="year" id="year" size="4" maxLength="4"></label>
          <label for="txtMonth">Month:<select name="month" id="month"></label>
            <option id="month1" value="1">Jan</option>
            <option id="month2" value="2">Feb</option>
            <option id="month3" value="3">Mar</option>
            <option id="month4" value="4">Apr</option>
            <option id="month5" value="5">May</option>
            <option id="month6" value="6">Jun</option>
            <option id="month7" value="7">Jul</option>
            <option id="month8" value="8">Aug</option>
            <option id="month9" value="9">Sep</option>
            <option id="month10" value="10">Oct</option>
            <option id="month11" value="11">Nov</option>
            <option id="month12" value="12">Dec</option>
               </select>
          <label for="txtDay">Day: <select name="day" id="day"></select>&nbsp;</td>
          </label>
        </td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>
          <input type="submit" value="SUBMIT" name="SUBMIT" id="btnSubmit">&nbsp;
          <input type="reset" value="CLEAR" id="btnReset">
        </td>
        <td>&nbsp;</td>
      </tr>
      </table>
      </div>
      </div>
      </div>
      </div>
      </div>
     </form>
  </body>
</html>

@endsection
