@extends('layouts.app')
@section('content')

<div class="row">
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="pull-right">
            </div>
            <i class="ion ion-ios-people"></i>&nbsp;Clients List
        </div>
        <div class="panel-body">
            <div class="table-responsive">
            <table class="table table-hover table-bordered">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Gender</th>
                        <th>Phone</th>
                        <th>Email</th>
                        <th>Address</th>
                        <th>Nationality</th>
                        <th>EducationBackground</th>
                        <th>DOB</th>
                        <th>Preferred mode of contact</th>
                    </tr>
                </thead>

            </table>
            </div>
        </div>
    </div>
</div>
@endsection